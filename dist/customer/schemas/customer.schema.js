"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const helper_1 = require("../../helper");
exports.CustomerSchema = new mongoose.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, validate: [helper_1.validateEmail, 'Please provide a valid email address'] },
    phone: { type: String, required: true, validate: [helper_1.validatePhone, 'Please provide a valid phone number'] },
    address: { type: String, required: true },
    description: { type: String },
    createdAt: { type: Date, default: Date.now },
});
//# sourceMappingURL=customer.schema.js.map