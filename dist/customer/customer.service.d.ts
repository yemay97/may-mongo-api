import { Model } from 'mongoose';
import { Customer } from './interfaces/customer.interface';
import { CreateCustomerDTO } from './dto/create-customer.dto';
export declare class CustomerService {
    private readonly customerModel;
    constructor(customerModel: Model<Customer>);
    getAllCustomer(): Promise<any>;
    getCustomer(customerID: any): Promise<any>;
    addCustomer(createCustomerDTO: CreateCustomerDTO): Promise<any>;
    updateCustomer(customerID: any, createCustomerDTO: CreateCustomerDTO): Promise<any>;
    deleteCustomer(customerID: any): Promise<any>;
    filterCustomer(filters: any): Promise<any>;
    filterCustomerByName(name: any): Promise<any>;
    countCustomer(filters: any): Promise<any>;
}
