"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let CustomerService = class CustomerService {
    constructor(customerModel) {
        this.customerModel = customerModel;
    }
    async getAllCustomer() {
        try {
            const customers = await this.customerModel.find();
            return { status: 'Success', result: customers };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async getCustomer(customerID) {
        try {
            const result = await this.customerModel.findById(customerID);
            return { status: 'Success', result };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async addCustomer(createCustomerDTO) {
        try {
            const newCustomer = new this.customerModel(createCustomerDTO);
            const result = await newCustomer.save();
            return { status: 'Success', result };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async updateCustomer(customerID, createCustomerDTO) {
        try {
            const updatedCustomer = await this.customerModel.findByIdAndUpdate(customerID, createCustomerDTO, { new: true, runValidators: true });
            return { status: 'Success', result: updatedCustomer };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async deleteCustomer(customerID) {
        try {
            const deletedCustomer = await this.customerModel.findByIdAndRemove(customerID);
            return { status: 'Success', result: deletedCustomer };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async filterCustomer(filters) {
        try {
            const customers = await this.customerModel
                .find({
                $and: [
                    { email: new RegExp(filters.email) },
                    { phone: new RegExp(filters.phone) },
                    { address: new RegExp(filters.address) },
                    { createdAt: { $gte: filters.minDate ? filters.minDate : '2000-01-01', $lte: filters.maxDate ? filters.maxDate : '3000-01-01' } },
                ],
            });
            return { status: 'Success', result: customers };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async filterCustomerByName(name) {
        try {
            const customers = await this.customerModel
                .find({
                $or: [{ firstName: new RegExp(name, 'i') },
                    { lastName: new RegExp(name, 'i') }],
            });
            return { status: 'Success', result: customers };
        }
        catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
    async countCustomer(filters) {
        const results = await this.customerModel
            .find({
            $and: [
                { email: new RegExp(filters.email) },
                { phone: new RegExp(filters.phone) },
                { address: new RegExp(filters.address) },
                { createdAt: { $gte: filters.minDate ? filters.minDate : '2000-01-01', $lte: filters.maxDate ? filters.maxDate : '3000-01-01' } },
                {
                    $or: [{ firstName: new RegExp(filters.customerName, 'i') },
                        { lastName: new RegExp(filters.customerName, 'i') }],
                },
            ],
        }).count();
        return results;
    }
};
CustomerService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Customer')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], CustomerService);
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map