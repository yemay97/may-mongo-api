export declare class CreateUserDTO {
    readonly id: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly userName: string;
    readonly email: string;
    readonly userRole: string;
    readonly status: string;
    readonly lastLogin: Date;
    readonly lastLoginStatus: string;
    readonly createdAt: Date;
}
