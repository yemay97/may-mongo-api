import { CustomerService } from './customer.service';
import { CreateCustomerDTO } from './dto/create-customer.dto';
export declare class CustomerController {
    private customerService;
    constructor(customerService: CustomerService);
    addCustomer(res: any, createCustomerDTO: CreateCustomerDTO): Promise<any>;
    getAllCustomer(res: any): Promise<any>;
    getFilterCutomer(res: any, filters: any): Promise<any>;
    getFilterCustomerByName(res: any, name: any): Promise<any>;
    getCountCustomer(res: any, criterias: any): Promise<any>;
    getCustomer(res: any, customerID: any): Promise<any>;
    updateCustomer(res: any, customerID: any, createCustomerDTO: CreateCustomerDTO): Promise<any>;
    deleteCustomer(res: any, customerID: any): Promise<any>;
}
