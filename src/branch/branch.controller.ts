import {
    Controller,
    Get,
    Res,
    HttpStatus,
    Post,
    Body,
    Put,
    Query,
    Delete,
} from '@nestjs/common';
import { BranchService } from './branch.service';
import { BranchDTO } from './dto/branch.dto';

@Controller('branch')
export class BranchController {
    constructor(private branchService: BranchService) { }

    @Post()
    async addBranch(@Res() res, @Body() branchDTO: BranchDTO) {
        const msg = await this.branchService.addBranch(branchDTO);
        return res.status(HttpStatus.OK).json(msg);
    }

    @Get()
    async getAllBranches(@Res() res) {
        const msg = await this.branchService.getAllBranches();
        return res.status(HttpStatus.OK).json(msg);
    }

    @Put('/update')
    async updateBranch(
        @Res() res,
        @Query('branchId') id,
        @Body() branchDTO: BranchDTO,
    ) {
        const msg = await this.branchService.updateBranch(
            id,
            branchDTO,
        );
        return res.status(HttpStatus.OK).json(msg);
    }

    @Delete('/delete')
    async deleteBranch(@Res() res, @Query('branchId') dId) {
        const msg = await this.branchService.deleteBranch(dId);
        return res.status(HttpStatus.OK).json(msg);
    }

}
