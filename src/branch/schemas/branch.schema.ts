import * as mongoose from 'mongoose';

export const BranchSchema = new mongoose.Schema({
    branchName: { type: String, required: true },
});
