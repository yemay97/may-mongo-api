import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Branch } from './interfaces/branch.interface';
import { BranchDTO } from './dto/branch.dto';

@Injectable()
export class BranchService {
    constructor(@InjectModel('Branch') private readonly branchModel: Model<Branch>) {
    }

    async getAllBranches(): Promise<any> {
        try {
            const branches = await this.branchModel.find();
            return { status: 'Success', result: branches };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async addBranch(branchDTO: BranchDTO): Promise<any> {
        try {
            const newBranch = new this.branchModel(branchDTO);
            const result = await newBranch.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async updateBranch(
        branchId,
        branchDTO: BranchDTO,
    ): Promise<any> {
        try {
            const updatedBranch = await this.branchModel.findByIdAndUpdate(
                branchId,
                branchDTO,
                { new: true, runValidators: true },
            );
            return { status: 'Success', result: updatedBranch };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async deleteBranch(branchId): Promise<any> {
        try {
            const deletedBranch = await this.branchModel.findByIdAndRemove(
                branchId,
            );
            return { status: 'Success', result: deletedBranch };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

}
