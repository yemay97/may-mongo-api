import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { port } from './env';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // By default, it is forbidden for two separate
  // application on different ports to interact or
  // share resources with each other unless it is otherwise allowed by one of them,

  // *CORS to allow request from the client side (for device with different port) from an Ionic application to a NestJS server
  app.enableCors();
  app.setGlobalPrefix('/api');
  await app.listen(process.env.PORT || port);
}
bootstrap();
