import { Document } from 'mongoose';
// used for type-checking and to determine the type of
// values that will be received by the application

export interface User extends Document {
  readonly firstName: string;
  readonly lastName: string;
  readonly username: string;
  readonly email: string;
  // Manager/ normal
  readonly userRole: string;
  readonly division: string;
  // account status: Active / Inactive
  readonly status: string;
  readonly lastLogin: Date;
  // failed/ Success
  readonly lastLoginStatus: string;
  readonly createdAt: Date;
}
