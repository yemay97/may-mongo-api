import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Query,
  NotFoundException,
  Delete,
  Param,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDTO } from './dto/user.dto';

@Controller('user')
export class UserController {
  // In order to interact with the database, the UserService was injected into the controller
  constructor(private userService: UserService) { }
  // add a user
  @Post()
  async addUser(@Res() res, @Body() userDTO: UserDTO) {
    const msg = await this.userService.addUser(userDTO);
    return res.status(HttpStatus.OK).json(msg);
  }

  // Retrieve users list
  @Get()
  async getAllUser(@Res() res) {
    const msg = await this.userService.getAllUser();
    return res.status(HttpStatus.OK).json(msg);
  }

  // filter by passing in body
  @Put('/get-filter-user')
  async getFilterUser(@Res() res, @Body() filters: any) {
    const msg = await this.userService.filterUser(filters);
    return res.status(HttpStatus.OK).json(msg);
  }

  // filter by name ex: http://localhost:3000/api/user/get-filter-user-by-name?name=o
  @Get('/get-filter-user-by-name')
  async getFilterUserByName(@Res() res, @Query('name') name) {
    const msg = await this.userService.filterUserByName(name);
    return res.status(HttpStatus.OK).json(msg);
  }

  // Fetch a particular user using ID
  @Get(':userID')
  async getUser(@Res() res, @Param('userID') userID) {
    const msg = await this.userService.getUser(userID);
    return res.status(HttpStatus.OK).json(msg);
  }

  // Update a user's details
  @Put('/update')
  async updateUser(
    @Res() res,
    @Query('userId') userID,
    @Body() userDTO: UserDTO,
  ) {
    const msg = await this.userService.updateUser(
      userID,
      userDTO,
    );
    return res.status(HttpStatus.OK).json(msg);
  }

  // Delete a user
  @Delete('/delete')
  async deleteUser(@Res() res, @Query('userId') userID) {
    const msg = await this.userService.deleteUser(userID);
    return res.status(HttpStatus.OK).json(msg);
  }

}
