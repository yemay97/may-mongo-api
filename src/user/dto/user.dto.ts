// A data transfer object will define how data will be sent on over the network.

export class UserDTO {
  readonly firstName: string;
  readonly lastName: string;
  readonly username: string;
  readonly email: string;
  // Manager/ normal
  readonly userRole: string;
  readonly division: string;
  // account status: Active / Inactive
  readonly status: string;
  readonly lastLogin: Date;
  // failed/ Success
  readonly lastLoginStatus: string;
  readonly createdAt: Date;
}
