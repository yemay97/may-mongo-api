import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { UserDTO } from './dto/user.dto';

@Injectable()
export class UserService {
  constructor(
    // used the @InjectModel method to inject the Customer model into the CustomerService class.
    // seamlessly carry out several database related activities such as, creating a customer
    @InjectModel('User') private readonly userModel: Model<User>,
  ) { }
  // fetch all users
  async getAllUser(): Promise<any> {
    try {
      const users = await this.userModel.find();
      return { status: 'Success', result: users };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Get a single customer
  async getUser(userId): Promise<any> {
    try {
      const result = await this.userModel.findById(userId);
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }
  // post a single customer
  async addUser(userDTO: UserDTO): Promise<any> {
    try {
      const newUser = new this.userModel(userDTO);
      const result = await newUser.save();
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Edit customer details
  async updateUser(
    userId,
    userDTO: UserDTO,
  ): Promise<any> {
    try {
      const updatedUser = await this.userModel.findByIdAndUpdate(
        userId,
        userDTO,
        { new: true, runValidators: true },
      );
      return { status: 'Success', result: updatedUser };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Delete a customer
  async deleteUser(userId): Promise<any> {
    try {
      const deletedUser = await this.userModel.findByIdAndRemove(
        userId,
      );
      return { status: 'Success', result: deletedUser };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // filter users by email,phone,address, minDate~ & ~maxDate
  async filterUser(filters): Promise<any> {
    try {
      const users = await this.userModel
        .find({
          $and: [
            { email: new RegExp(filters.email) },
            // gte - greater, lte- lesser
            { createdAt: { $gte: filters.minDate ? filters.minDate : '2000-01-01', $lte: filters.maxDate ? filters.maxDate : '3000-01-01' } },
            // { $or: [{ firstName: new RegExp(filters.customerName, 'i') },
            // { lastName: new RegExp(filters.customerName, 'i') }] },
          ],
        })
        ;
      return { status: 'Success', result: users };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  async filterUserByName(name): Promise<any> {
    try {
      const users = await this.userModel
        .find({
          $or: [{ firstName: new RegExp(name, 'i') },
          { lastName: new RegExp(name, 'i') }],
        },
        );
      return { status: 'Success', result: users };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }
}
