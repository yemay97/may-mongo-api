import * as mongoose from 'mongoose';
import { validateEmail } from '../../helper';

export const UserSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  username: { type: String, required: true },
  userRole: {
    type: String, required: true, enum: {
      values: ['Head Division', 'Referrer'],
      message: 'Please use Head Division or Referrer for role',
    },
  },
  division: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Division', required: true,
  },
  status: {
    type: String, required: true, enum: {
      values: ['Active', 'Inactive'],
      message: 'Please use Active or Inactive for status',
    },
  },
  lastLogin: { type: Date },
  lastLoginStatus: {
    type: String, enum: {
      values: ['Success', 'Failed'],
      message: 'Please use Success or Failed for last login status',
    },
  },
  email: { type: String, required: true, validate: [validateEmail, 'Please provide a valid email address'] },
  // phone: { type: String, required: true, validate: [validatePhone, 'Please provide a valid phone number'] },
  createdAt: { type: Date, default: Date.now },
});
