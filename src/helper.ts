export const validateEmail = email => {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
  };

export const validatePhone = phone => {
    const malaysianum = /^(\+?6?01)[0-46-9]-*[0-9]{7,8}$/;
    return malaysianum.test(phone);
  };
