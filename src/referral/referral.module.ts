import { Module } from '@nestjs/common';
import { ReferralController } from './referral.controller';
import { ReferralService } from './referral.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ReferralSchema } from './schemas/referral.schema';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Referral', schema: ReferralSchema }]),
  ],
  controllers: [ReferralController],
  providers: [ReferralService],
})
export class ReferralModule { }
