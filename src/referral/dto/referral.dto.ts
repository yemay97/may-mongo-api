import * as mongoose from 'mongoose';

// A data transfer object will define how data will be sent on over the network.

export class ReferralDTO {
  readonly id: string;
  readonly name: string;
  readonly phone: string;
  readonly email: string;
  readonly division: string;
  readonly branch: string;
  readonly productType: string;
  readonly product: string;
  readonly status: string;
  readonly FUM: number;
  // referrer -> user's id
  readonly referrer: string;

  readonly remarks: string;

  readonly createdAt: Date;
}
