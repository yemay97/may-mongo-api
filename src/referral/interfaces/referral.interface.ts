import { Document } from 'mongoose';
// used for type-checking and to determine the type of values that will be received by the application

export interface Referral extends Document {
  readonly id: string;
  readonly name: string;
  readonly phone: string;
  readonly email: string;
  readonly division: string;
  readonly branch: string;
  readonly productType: string;
  readonly product: string;
  readonly status: string;
  readonly FUM: number;
  // referrer -> user's id
  readonly referrer: string;
  readonly remarks: string;
  readonly createdAt: Date;
}
