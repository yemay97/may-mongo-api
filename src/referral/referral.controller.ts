import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  Query,
  Param,
} from '@nestjs/common';
import { ReferralService } from './referral.service';
import { ReferralDTO } from './dto/referral.dto';

@Controller('referral')
export class ReferralController {
  // to interact with database
  constructor(private referralService: ReferralService) { }

  // add a referral
  @Post()
  async addReferral(@Res() res, @Body() referralDTO: ReferralDTO) {
    const msg = await this.referralService.addReferral(referralDTO);
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get()
  async getAllReferral(@Res() res) {
    const msg = await this.referralService.getAllReferral();
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get('/get-referral-by-user/:ID')
  async getReferralByUser(@Res() res, @Param('ID') id) {
    const msg = await this.referralService.getReferralByUser(id);
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get('/get-latest-ra-by-user/:ID')
  async getLatestReferralActivitiesByUser(@Res() res, @Param('ID') id) {
    const msg = await this.referralService.getLatestReferralActivities(id);
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get('/get-dashboard-data-by-user/:ID')
  async getDashboardDataByUser(@Res() res, @Param('ID') id) {
    const msg = await this.referralService.getDashboardDataByUser(
      id,
    );
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get('/get-dashboard-data-by-division/:divisionId')
  async getDashboardData(@Res() res, @Param('divisionId') id) {
    const msg = await this.referralService.getDashboardDataByDivision(id);
    return res.status(HttpStatus.OK).json(msg);
  }

  @Get(':ID')
  async getReferral(@Res() res, @Param('ID') referralID) {
    const msg = await this.referralService.getReferral(referralID);
    return res.status(HttpStatus.OK).json(msg);
  }

  // example : http://localhost:3000/api/referral/update?id=5da7c4f9c6d72707464f4ce8
  @Put('/update')
  async updateReferral(
    @Res() res,
    @Query('id') id,
    @Body() referralDTO: ReferralDTO,
  ) {
    const msg = await this.referralService.updateReferral(
      id,
      referralDTO,
    );
    return res.status(HttpStatus.OK).json(msg);
  }

  @Put('/get-filter-referral')
  async filterReferral(
    @Res() res,
    @Body() filter: any,
  ) {
    const msg = await this.referralService.getFilterReferral(
      filter,
    );
    return res.status(HttpStatus.OK).json(msg);
  }
}
