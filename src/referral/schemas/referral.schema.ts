import * as mongoose from 'mongoose';
import { validateEmail, validatePhone } from '../../helper';

export const ReferralSchema = new mongoose.Schema({

  id: { type: String },
  name: { type: String, required: true },
  email: { type: String, validate: [validateEmail, 'Please provide a valid email address'] },
  phone: { type: String, required: true, validate: [validatePhone, 'Please provide a valid phone number'] },
  division: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Division', required: true,
  },
  branch: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Branch', required: true,
  },
  productType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ProductType', required: true,
  },
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product', required: true,
  },
  remarks: String,
  status: {
    type: String, required: true, enum: {
      values: ['In Progress', 'Successful', 'Unsuccessful', 'Invalid'],
      message: 'Please use In progress, Successful, Unsuccessful or Invalid for status',
    },
  },
  referrer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', required: true,
  },
  FUM: { type: Number, default: 0 },
  createdAt: { type: Date, default: Date.now },
});
