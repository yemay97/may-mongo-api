import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Referral } from './interfaces/referral.interface';
import { ReferralDTO } from './dto/referral.dto';

@Injectable()
export class ReferralService {
  constructor(
    // used the @InjectModel method to inject the Referral model into the ReferralService class.
    // seamlessly carry out several database related activities such as, creating a Referral
    @InjectModel('Referral') private readonly referralModel: Model<Referral>,
  ) { }

  async addReferral(referralDTO: ReferralDTO): Promise<any> {
    const newReferral = new this.referralModel(referralDTO);
    try {
      const result = await newReferral.save();
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // populate referral's id to referral's data
  async getAllReferral(): Promise<any> {
    try {
      const result = await this.referralModel.find().populate('referrer division branch productType product',
        'branchName typeName productName divisionName firstName lastName email');
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Get a single referral
  async getReferral(id: string): Promise<any> {
    try {
      const result = await this.referralModel.findById(id).populate('referrer division branch productType product',
        'branchName typeName productName divisionName firstName lastName email');
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Get all referrals under a Referrer
  async getReferralByUser(id: string): Promise<any> {
    try {
      const result = await this.referralModel.find({ referrer: id }).populate('referrer division branch productType product',
        'branchName typeName productName divisionName firstName lastName email');
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Edit referral details
  async updateReferral(
    id,
    referralDTO: ReferralDTO,
  ): Promise<any> {
    try {
      const result = await this.referralModel.findByIdAndUpdate(
        id,
        referralDTO,
        { new: true, runValidators: true },
      );
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // Get top 3 latest created referral based on userId
  async getLatestReferralActivities(userId: string): Promise<any> {
    try {
      const result = await this.referralModel.find({ referrer: userId }, ['name', 'status', 'createdAt'], {
        skip: 0, // Starting Row
        limit: 3, // Ending Row
        sort: {
          createdAt: -1, // Sort by Date Created DESC
        },
      }).populate('referrer', 'firstName lastName');
      return { status: 'Success', result };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // filter by branch, division, type, product, minDate, maxDate, status
  async getFilterReferral(filters: any): Promise<any> {
    const filterOption = [];
    try {
      filterOption.push({
        createdAt:
          { $gte: filters.minDate ? filters.minDate : '2000-01-01', $lte: filters.maxDate ? filters.maxDate : '3000-01-01' },
      });
      if (filters.status) {
        filterOption.push({ status: filters.status });
      }
      if (filters.branch) {
        filterOption.push({ branch: filters.branch });
      }
      if (filters.division) {
        filterOption.push({ division: filters.division });
      }
      if (filters.productType) {
        filterOption.push({ productType: filters.productType });
      }
      if (filters.product) {
        filterOption.push({ product: filters.product });
      }
      const referrals = await this.referralModel
        .find({
          $and: filterOption,
        }).populate('referrer division branch productType product',
          'branchName typeName productName divisionName firstName lastName email');
      return { status: 'Success', result: referrals };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // total FUM of referrals based on Referrer (Total, Successful, Unsuccessful, In Progress)
  async getDashboardDataByUser(userId: string): Promise<any> {
    try {
      const id = mongoose.Types.ObjectId(userId);
      // group by referrerID, status => get totalFum, success, unsuccess, inprogress
      const bystatus = await this.referralModel.aggregate([
        { $match: { referrer: id } },
        {
          $group: {
            _id: { status: '$status' }, status: { $first: '$status' }, totalFum: { $sum: '$FUM' },
            totalReferral: { $sum: 1 },
          },
        }, {
          $project: {
            _id: 0,
          },
        },
      ]);

      const totalData = await this.referralModel.aggregate([
        // group by referrerID => get totalFum,totalReferrals
        { $match: { referrer: id } },
        {
          $group: {
            _id: { customerId: '$referrer' }, totalFum: { $sum: '$FUM' },
            totalReferral: { $sum: 1 },
          },
        }, {
          $project: {
            _id: 0,
          },
        }]);
      return { status: 'Success', result: { bystatus, totalData } };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }

  // total FUM of referrals based on DIVISION (Total, Successful, Unsuccessful, In Progress)
  async getDashboardDataByDivision(divisionId: string): Promise<any> {
    try {
      const divisionid = mongoose.Types.ObjectId(divisionId);
      // group by referrerID, status => get totalFum, success, unsuccess, inprogress
      const bystatus = await this.referralModel.aggregate([
        { $match: { division: divisionid } },
        {
          $group: {
            _id: { status: '$status' }, status: { $first: '$status' }, totalFum: { $sum: '$FUM' },
            totalReferral: { $sum: 1 },
          },
        }, {
          // hide displaying _id
          $project: {
            _id: 0,
          },
        },
      ]);

      const totalData = await this.referralModel.aggregate([
        // group by referrerID => get totalFum,totalReferrals
        { $match: { division: divisionid } },
        {
          $group: {
            _id: { id: null }, totalFum: { $sum: '$FUM' },
            totalReferral: { $sum: 1 },
          },
        }, {
          // hide displaying _id
          $project: {
            _id: 0,
          },
        }]);
      return { status: 'Success', result: { bystatus, totalData } };
    } catch (err) {
      return { status: 'Failed', message: err.message };
    }
  }
}
