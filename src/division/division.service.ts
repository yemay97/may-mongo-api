import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Division } from './interfaces/division.interface';
import { DivisionDTO } from './dto/division.dto';

@Injectable()
export class DivisionService {
    constructor(@InjectModel('Division') private readonly divisionModel: Model<Division>) {
    }

    async getAllDivisions(): Promise<any> {
        try {
            const divisions = await this.divisionModel.find();
            return { status: 'Success', result: divisions };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async addDivision(divisionDTO: DivisionDTO): Promise<any> {
        try {
            const newDivision = new this.divisionModel(divisionDTO);
            const result = await newDivision.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async updateDivision(
        divisionId,
        divisionDTO: DivisionDTO,
    ): Promise<any> {
        try {
            const updatedDivision = await this.divisionModel.findByIdAndUpdate(
                divisionId,
                divisionDTO,
                { new: true, runValidators: true },
            );
            return { status: 'Success', result: updatedDivision };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async deleteDivision(divisionId): Promise<any> {
        try {
            const deletedDivision = await this.divisionModel.findByIdAndRemove(
                divisionId,
            );
            return { status: 'Success', result: deletedDivision };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

}
