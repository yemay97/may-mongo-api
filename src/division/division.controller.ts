import {
    Controller,
    Get,
    Res,
    HttpStatus,
    Post,
    Body,
    Put,
    Query,
    Delete,
} from '@nestjs/common';
import { DivisionService } from './division.service';
import { DivisionDTO } from './dto/division.dto';

@Controller('division')
export class DivisionController {
    constructor(private divisionService: DivisionService) { }

    @Post()
    async addDivision(@Res() res, @Body() divisionDTO: DivisionDTO) {
        const msg = await this.divisionService.addDivision(divisionDTO);
        return res.status(HttpStatus.OK).json(msg);
    }

    @Get()
    async getAllDivisions(@Res() res) {
        const msg = await this.divisionService.getAllDivisions();
        return res.status(HttpStatus.OK).json(msg);
    }

    @Put('/update')
    async updateDivision(
        @Res() res,
        @Query('divisionId') dId,
        @Body() divisionDTO: DivisionDTO,
    ) {
        const msg = await this.divisionService.updateDivision(
            dId,
            divisionDTO,
        );
        return res.status(HttpStatus.OK).json(msg);
    }

    @Delete('/delete')
    async deleteDivision(@Res() res, @Query('divisionId') dId) {
        const msg = await this.divisionService.deleteDivision(dId);
        return res.status(HttpStatus.OK).json(msg);
    }

}
