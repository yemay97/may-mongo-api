import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DivisionSchema } from './schemas/division.schema';
import { DivisionService } from './division.service';
import { DivisionController } from './division.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Division', schema: DivisionSchema }]),
  ],
  controllers: [DivisionController],
  providers: [DivisionService],
})
export class DivisionModule { }
