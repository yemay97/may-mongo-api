import { Document } from 'mongoose';

export interface Division extends Document {
    readonly id: string;
    readonly divisionName: string;
}
