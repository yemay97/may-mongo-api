import * as mongoose from 'mongoose';

export const DivisionSchema = new mongoose.Schema({
    id: { type: String, required: true },
    divisionName: { type: String, required: true },
});
