export class DivisionDTO {
    readonly id: string;
    readonly divisionName: string;
}
