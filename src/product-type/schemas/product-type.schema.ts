import * as mongoose from 'mongoose';

export const ProductTypeSchema = new mongoose.Schema({
    typeName: { type: String, required: true },
    division: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Division', required: true,
    },
});
