import { Document } from 'mongoose';

export interface ProductType extends Document {
    readonly typeName: string;
    readonly division: string;
}
