import {
    Controller,
    Get,
    Res,
    HttpStatus,
    Post,
    Body,
    Put,
    Query,
    Delete,
} from '@nestjs/common';
import { ProductTypeService } from './product-type.service';
import { ProductTypeDTO } from './dto/product-type.dto';

@Controller('product-type')
export class ProductTypeController {
    constructor(private productTypeService: ProductTypeService) { }

    @Post()
    async addProductType(@Res() res, @Body() productTypeDTO: ProductTypeDTO) {
        const msg = await this.productTypeService.addProductType(productTypeDTO);
        return res.status(HttpStatus.OK).json(msg);
    }

    @Get()
    async getAllProductTypes(@Res() res) {
        const msg = await this.productTypeService.getAllProductTypes();
        return res.status(HttpStatus.OK).json(msg);
    }

    @Put('/update')
    async updateProductType(
        @Res() res,
        @Query('id') dId,
        @Body() productTypeDTO: ProductTypeDTO,
    ) {
        const msg = await this.productTypeService.updateProductType(
            dId,
            productTypeDTO,
        );
        return res.status(HttpStatus.OK).json(msg);
    }

    @Delete('/delete')
    async deleteProductType(@Res() res, @Query('id') dId) {
        const msg = await this.productTypeService.deleteProductType(dId);
        return res.status(HttpStatus.OK).json(msg);
    }

}
