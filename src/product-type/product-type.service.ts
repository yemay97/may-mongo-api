import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ProductType } from './interfaces/product-type.interface';
import { ProductTypeDTO } from './dto/product-type.dto';

@Injectable()
export class ProductTypeService {
    constructor(@InjectModel('ProductType') private readonly productTypeModel: Model<ProductType>) {
    }

    async getAllProductTypes(): Promise<any> {
        try {
            const productTypes = await this.productTypeModel.find().populate('division');
            return { status: 'Success', result: productTypes };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async addProductType(productTypeDTO: ProductTypeDTO): Promise<any> {
        try {
            const newProductType = new this.productTypeModel(productTypeDTO);
            const result = await newProductType.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async updateProductType(
        id,
        productTypeDTO: ProductTypeDTO,
    ): Promise<any> {
        try {
            const updatedProductType = await this.productTypeModel.findByIdAndUpdate(
                id,
                productTypeDTO,
                { new: true, runValidators: true },
            );
            return { status: 'Success', result: updatedProductType };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async deleteProductType(id): Promise<any> {
        try {
            const deletedProductType = await this.productTypeModel.findByIdAndRemove(
                id,
            );
            return { status: 'Success', result: deletedProductType };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

}
