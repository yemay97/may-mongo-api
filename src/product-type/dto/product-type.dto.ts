export class ProductTypeDTO {
    readonly typeName: string;
    readonly division: string;
}
