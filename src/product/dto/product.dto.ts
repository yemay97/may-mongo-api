export class ProductDTO {
    readonly productName: string;
    readonly productType: string;
}
