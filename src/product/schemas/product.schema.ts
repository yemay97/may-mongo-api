import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({

    productName: { type: String, required: true },
    productType: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductType', required: true,
    },
});
