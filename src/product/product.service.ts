import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Product } from './interfaces/product.interface';
import { ProductDTO } from './dto/product.dto';

@Injectable()
export class ProductService {
    constructor(@InjectModel('Product') private readonly productModel: Model<Product>) {
    }

    async getAllProducts(): Promise<any> {
        try {
            const products = await this.productModel.find().populate('productType');
            return { status: 'Success', result: products };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async addProduct(productDTO: ProductDTO): Promise<any> {
        try {
            const newProduct = new this.productModel(productDTO);
            const result = await newProduct.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async updateProduct(
        id,
        productDTO: ProductDTO,
    ): Promise<any> {
        try {
            const updatedProduct = await this.productModel.findByIdAndUpdate(
                id,
                productDTO,
                { new: true, runValidators: true },
            );
            return { status: 'Success', result: updatedProduct };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    async deleteProduct(id): Promise<any> {
        try {
            const deletedProduct = await this.productModel.findByIdAndRemove(
                id,
            );
            return { status: 'Success', result: deletedProduct };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

}
