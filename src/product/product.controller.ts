import {
    Controller,
    Get,
    Res,
    HttpStatus,
    Post,
    Body,
    Put,
    Query,
    Delete,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductDTO } from './dto/product.dto';

@Controller('product')
export class ProductController {
    constructor(private productService: ProductService) { }

    @Post()
    async addProduct(@Res() res, @Body() productDTO: ProductDTO) {
        const msg = await this.productService.addProduct(productDTO);
        return res.status(HttpStatus.OK).json(msg);
    }

    @Get()
    async getAllProducts(@Res() res) {
        const msg = await this.productService.getAllProducts();
        return res.status(HttpStatus.OK).json(msg);
    }

    @Put('/update')
    async updateProduct(
        @Res() res,
        @Query('id') id,
        @Body() productDTO: ProductDTO,
    ) {
        const msg = await this.productService.updateProduct(
            id,
            productDTO,
        );
        return res.status(HttpStatus.OK).json(msg);
    }

    @Delete('/delete')
    async deleteProduct(@Res() res, @Query('id') dId) {
        const msg = await this.productService.deleteProduct(dId);
        return res.status(HttpStatus.OK).json(msg);
    }

}
