import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { uri } from './env';
import { ReferralModule } from './referral/referral.module';
import { DivisionModule } from './division/division.module';
import { BranchModule } from './branch/branch.module';
import { ProductTypeModule } from './product-type/product-type.module';
import { ProductModule } from './product/product.module';

@Module({
  imports: [
    // Mongoose module for MongoDB uses the forRoot() method to supply the connection to the database.
    MongooseModule.forRoot(
      uri,
      // 'mongodb://localhost/customer-app',
      {
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    ),
    UserModule,
    ReferralModule,
    DivisionModule,
    BranchModule,
    ProductTypeModule, ProductModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
